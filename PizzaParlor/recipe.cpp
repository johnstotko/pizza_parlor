#include "recipe.h"
#include <map>
#include <string>
#include <vector>
#include <cstdlib>

using namespace std;

Recipe::Recipe(){
		
	ingred.push_back("Flour");
	smallUnit[ingred.back()] = "g";
	bulkUnit[ingred.back()] = "kg";
	marketToPlayerUnitConvert[ingred.back()] = 1000.0;
	abbreviation["F"] = ingred.back();

	ingred.push_back("Yeast");
	smallUnit[ingred.back()] = "g";
	bulkUnit[ingred.back()] = "kg";
	marketToPlayerUnitConvert[ingred.back()] = 1000.0;
	abbreviation["Y"] = ingred.back();

	ingred.push_back("Water");
	smallUnit[ingred.back()] = "mL";
	bulkUnit[ingred.back()] = "L";
	marketToPlayerUnitConvert[ingred.back()] = 1000.0;
	abbreviation["W"] = ingred.back();

	ingred.push_back("Tomatoes");
	smallUnit[ingred.back()] = "count";
	bulkUnit[ingred.back()] = "count";
	marketToPlayerUnitConvert[ingred.back()] = 1.0;
	abbreviation["T"] = ingred.back();

	ingred.push_back("Garlic");
	smallUnit[ingred.back()] = "g";
	bulkUnit[ingred.back()] = "kg";
	marketToPlayerUnitConvert[ingred.back()] = 1000.0;
	abbreviation["G"] = ingred.back();

	ingred.push_back("Salt");
	smallUnit[ingred.back()] = "g";
	bulkUnit[ingred.back()] = "kg";
	marketToPlayerUnitConvert[ingred.back()] = 1000.0;
	abbreviation["S"] = ingred.back();

	ingred.push_back("Oregano");
	smallUnit[ingred.back()] = "g";
	bulkUnit[ingred.back()] = "kg";
	marketToPlayerUnitConvert[ingred.back()] = 1000.0;
	abbreviation["O"] = ingred.back();

	ingred.push_back("Mozzerella");
	smallUnit[ingred.back()] = "g";
	bulkUnit[ingred.back()] = "kg";
	marketToPlayerUnitConvert[ingred.back()] = 1000.0;
	abbreviation["M"] = ingred.back();

	ingred.push_back("Pepperoni");
	smallUnit[ingred.back()] = "g";
	bulkUnit[ingred.back()] = "kg";
	marketToPlayerUnitConvert[ingred.back()] = 1000.0;
	abbreviation["P"] = ingred.back();

	for (vector<string>::iterator in = ingred.begin(); in != ingred.end(); ++in) {
		amount[*in] = 0.0;
	}

}

string Recipe::getRandomIngredient() {

	return ingred.at(rand() % ingred.size());

}