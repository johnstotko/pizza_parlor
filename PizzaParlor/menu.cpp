#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class Menu
{
public:
	void display() {
		ifstream inFile("res/menus/mainmenu.txt");

		string line;
		while (std::getline(inFile, line)) {
			cout << line << endl;
		}
		inFile.close();
	}
};