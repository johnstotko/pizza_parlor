#pragma once
#ifndef MARKET_H
#define MARKET_H

#include <string>
#include <sstream>
#include <iomanip>
#include "recipe.h"

using namespace std;

class Market {
public:
	Recipe prices;

	string getIngredientListing(string ingredient);
	void driftPrices();

	Market();
};



#endif