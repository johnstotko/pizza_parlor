#include <string>
#include <map>
#include "recipe.h"

using namespace std;

class Customers {
public:

	Recipe idealRecipe;
	Recipe ingredientImpression;
	double idealPrice = 8.00;
	double popularity = 5;		// mean amount of people go to your shop per tick, 1 => you exist.
	double impression = 0;		// recipe performance for that day, effects likelyhood to buy
	double expectation = 0.5;	// recipe rank people expect, adjusts slowly
	double renown = 0;			// makes sure popularity doesn't go down

	double popularityMultiplier = 3;
	double expectationMultiplier = 0.01;
	double priceMultiplier;

	enum MarginLevels {PERFECT, A_LITTLE_TOO, TOO, WAY_TOO};
	map<MarginLevels, double> margin;

	void reflect(Recipe recipe, double price) {

		reflectPopularity();
		reflectExpectation();
		
	}

	void makeImpression(Recipe recipe, double price) {
		impression = 0;
		for (vector<string>::iterator ingredient = idealRecipe.ingred.begin(); ingredient != idealRecipe.ingred.end(); ++ingredient) {
			ingredientImpression.amount[*ingredient] = 1 - recipe.amount[*ingredient] / idealRecipe.amount[*ingredient];
			impression += abs(ingredientImpression.amount[*ingredient]);
		}

		if (price == 0.0) {
			price = 10^-6;
		}

		priceMultiplier = (1 / (1 + impression))*(idealPrice/price);


	}

	int howManyPizzaWerePurchased() {

		return (int)(popularity * (1 + (priceMultiplier/(1 + impression))));
;
	}

	void reflectExpectation() {
		expectation = expectationMultiplier*(expectation - impression);
	}

	void reflectPopularity() {

		popularity += popularityMultiplier*((2 / (1 + impression)) - 1);

		if (popularity < 1){
			popularity = 1;
		}

	}

	string ingredientFeedback(string ingredient) {

		string feedback = "There was ";

		if (ingredientImpression.amount[ingredient] < margin[A_LITTLE_TOO]) {
			feedback += "a tiny too ";
		}
		else if (ingredientImpression.amount[ingredient] < margin[TOO]) {
			feedback += "a little too ";
		}
		else if (ingredientImpression.amount[ingredient] < margin[WAY_TOO]) {
			feedback += "too ";
		}
		else {
			feedback += "way too ";
		}


		if (ingredientImpression.amount[ingredient] > 0){
			feedback += "little ";
		}
		else if (ingredientImpression.amount[ingredient] < 0) {
			feedback += "much ";
		}
		else {
			return "The " + ingredient + " was perfect!";
		}

		return feedback + ingredient;

	}
	
	string getCustomerCountListing() {
		string line = "|   You had " + to_string((int)popularity) + " customers today";

		while (line.length() < 45) {
			line += " ";
		}
		return line + "|";
	}

	string getFeedbackListing(string ingredient) {
		string line = "   " + ingredientFeedback(ingredient);

		while (line.length() < 72) {
			line += " ";
		}

		return line + "|";
	}
	
	Customers(){

		idealRecipe.amount["Flour"] = 467;
		idealRecipe.amount["Yeast"] = 7;
		idealRecipe.amount["Water"] = 326;
		idealRecipe.amount["Tomatoes"] = 7;
		idealRecipe.amount["Garlic"] = 12;
		idealRecipe.amount["Salt"] = 34;
		idealRecipe.amount["Oregano"] = 4;
		idealRecipe.amount["Mozzerella"] = 227;
		idealRecipe.amount["Pepperoni"] = 36;

		margin[PERFECT] = 0;
		margin[A_LITTLE_TOO] = 0.06;
		margin[TOO] = 0.10;
		margin[WAY_TOO] = 0.17;
	}

};