#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "market.h"
#include "player.cpp"
#include "recipe.h"
#include "customers.cpp"
#include "formathandler.h"
using namespace std;

class Game
{
public:

	enum State { EXIT, MAINMENU, NEWGAME, LOADGAME, OPTIONS, CREDITS, ADJUST, OPEN, SUMMARY };
	State currentState = MAINMENU;

	int pizzaPurchased;

	Market market;
	Player player;
	Recipe recipe;
	Customers customers;
	FormatHandler formathandler;

	void runGame() {

		switch (currentState) {
		case MAINMENU:	runMainMenu(); break;
		case NEWGAME:	runNewGame(); break;
		case LOADGAME:	runLoadGame();  break;
		case OPTIONS:	runOptions(); break;
		case CREDITS:	runCreditMenu(); break;
		case ADJUST:	runAdjust(); break;
		case OPEN:		runOpen(); break;
		case SUMMARY:	runSummary(); break;

		}

	}

	void runLoadGame() {
		display("res/coming-soon.txt");
		system("PAUSE");
		currentState = MAINMENU;
	}

	void runOptions() {
		display("res/coming-soon.txt");
		system("PAUSE");
		currentState = MAINMENU;
	}

	void runNewGame() {

		display("res/in-dev-warning.txt");
		system("pause");

		display("res/letterfromuncle.txt");
		system("pause");
		system("CLS");

		string response;
		string parlorName;

		cout << "\n \n      Do I want to take over my uncles pizza parlor? (Yes/No) \n \n     ";
		cin >> response;

		if (response != "Yes") {
			currentState = MAINMENU;
			return;
		}
		cout << "\n \n      What do I want to name it? Something... snazzy.. maybe... \n \n      ";

		while (parlorName == "") {
			cin >> parlorName;
		}

		cout << "\n \n      Yes.. I think that sounds good. Let's see what my uncle left me! \n \n \n \n \n    ";
		system("pause");
		currentState = ADJUST;
	}

	void runAdjust() {
		displayAdjustScreen();
		cout << "Select an option above, or type OPEN to open shop. \n";
		string input;
		cin >> input;

		displayAdjustScreen();

		double amount;

		string ingredient = recipe.abbreviation[input.substr(1, 1)];
		if (input.at(0) == 'R') {
			cout << "How many " + recipe.smallUnit[ingredient] + " of " + ingredient + " would you like to use?" << endl;
			// TODO: validate input
			cin >> player.recipe.amount[ingredient];

			currentState = ADJUST;

		}
		else if (input.at(0) == 'B') {
			cout << "How many " + recipe.bulkUnit[ingredient] + " of " + ingredient + " would you like to buy?" << endl;
			cin >> amount;
			displayAdjustScreen();

			// TODO: validate input
			cout << player.buyIngredient(ingredient, amount, market) << endl;
			system("pause");

			currentState = ADJUST;
		}
		else if (input == "$") {
			cout << "How much do you want to charge for a pizza?" << endl;
			// TODO: validate input
			cin >> player.price;

			currentState = ADJUST;
		}
		else if (input == "OPEN") {
			cout << "Are you sure you want to start the day? (Yes/No)" << endl;
			cin >> input;
			if (input == "Yes") {
				currentState = OPEN;
			}
			else {
				currentState = ADJUST;
			}
		}


	}

	void runMainMenu() {
		display("res/menus/mainmenu.txt");

		string input;
		cin >> input;

		if (input == "1") {
			currentState = NEWGAME;
		}
		else if (input == "2") {
			currentState = LOADGAME;
		}
		else if (input == "3") {
			currentState = OPTIONS;
		}
		else if (input == "4") {
			currentState = CREDITS;
		}
		else {
			runMainMenu();
		}

	}

	void runCreditMenu() {
		display("res/menus/credits.txt");
		system("PAUSE");
		runMainMenu();
	}

	void display(string filename) {
		system("CLS");
		ifstream inFile(filename);

		string line;
		while (std::getline(inFile, line)) {
			cout << line << endl;
		}
		inFile.close();
	}

	void displayAdjustScreen() {
		system("CLS");
		ifstream inFile("res/adjust-template.txt");

		int starNumber = 0;

		string line;
		while (std::getline(inFile, line)) {

			if (line == "*") {
				starNumber++;
				switch (starNumber) {
				case 1: {
					cout << "|    Recipe, makes 4 pizzas             |    Current Stock                      |   My cash: $" + player.getCashListing() + "|" << endl;
					break;
				}
				case 2: {

					for (int i = 0; i < 3; i++) {
						cout << adjustScreenLine(recipe.ingred[i]) << endl;
					}

					break;
				}
				case 3: {

					for (int i = 3; i < 7; i++) {
						cout << adjustScreenLine(recipe.ingred[i]) << endl;
					}

					break;
				}
				case 4: {

					for (int i = 7; i < 9; i++) {
						cout << adjustScreenLine(recipe.ingred[i]) << endl;
					}

					break;
				}
				case 5: {
					cout << "|                                       ";
					cout << "|                                       ";
					cout << "|  ($) My pizza price: $ " + player.getPriceListing() + "|" << endl;
				}
				}

			}
			else {
				cout << line << endl;
			}
		}
		inFile.close();


	}

	string adjustScreenLine(string ingredient) {
		return "|" + player.getRecipeListing(ingredient) + "|" + player.getStockListing(ingredient) + "|" + market.getIngredientListing(ingredient) + "|";
	}

	void runOpen() {

		display("res/closed.txt");
		Sleep(2000);
		display("res/open.txt");
		Sleep(1000);

		for (int i = 1; i < 7; i++) {
			display("res/day_" + to_string(i) + ".txt");
			Sleep(750);
		}

		customers.makeImpression(player.recipe, player.price);
		customers.reflect(player.recipe, player.price);

		currentState = SUMMARY;
	}

	void runSummary() {

		settleTheDay();

		displaySummaryScreen();

		system("Pause");
		currentState = ADJUST;
	}

	void settleTheDay() {


		int flag = -1;
		for (int i = 0; i < customers.howManyPizzaWerePurchased(); i++) {
			for (vector<string>::iterator in = recipe.ingred.begin(); in != recipe.ingred.end(); ++in) {
				if (player.stock.amount[*in]*recipe.marketToPlayerUnitConvert[*in] - player.recipe.amount[*in] * i/4 < 0) {
					flag = i;
				}
			}
		}

		if (flag != -1) {
			pizzaPurchased = customers.howManyPizzaWerePurchased();
		}

		for (vector<string>::iterator in = recipe.ingred.begin(); in != recipe.ingred.end(); ++in) {
			player.stock.amount[*in] += -player.recipe.amount[*in] * pizzaPurchased / (4 * recipe.marketToPlayerUnitConvert[*in]);
		}

		player.cash += player.price * pizzaPurchased;

	}

	void displaySummaryScreen() {

		system("CLS");

		ifstream inFile("res/summary-template.txt");

		int starNumber = 0;

		string line;

		string price;
		string amount;
		string total;


		while (std::getline(inFile, line)) {

			if (line == "*") {

				starNumber++;

				switch (starNumber){
				case 1: {
					cout << customers.getCustomerCountListing() + customers.getFeedbackListing(recipe.getRandomIngredient()) << endl;
					break;
				}
				case 2: {

					price = formathandler.doubleToDollar(player.price);

					while (price.length() < 6) {
						price = " " + price;
					}

					amount = to_string(pizzaPurchased);

					while (amount.length() < 9) {
						amount = " " + amount;
					}

					total = formathandler.doubleToDollar(player.price*customers.howManyPizzaWerePurchased());

					while (total.length() < 8) {
						total = " " + total;
					}

					cout << "|   Pizza   $" + price + " " + amount + "    $" + total + "   |" + customers.getFeedbackListing(recipe.getRandomIngredient()) << endl;
					break;
				}
				case 3: {
					cout << "|   Total sales                  $" + total + "   |" + customers.getFeedbackListing(recipe.getRandomIngredient()) << endl;
					break;
				}
				}

			}
			else {
				cout << line << endl;
			}
		}

	}

};