#include <iostream>
#include <fstream>
#include <string>
#include "game.cpp"
//#include "menu.cpp"
using namespace std;

int main() {

	system("MODE CON COLS=120 LINES=30");
	Game game;

	while (game.currentState != game.EXIT) {
		game.runGame();
	}

	system("pause");
	return 0;
}