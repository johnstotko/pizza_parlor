#include <iostream>
#include <sstream>
#include <iomanip>
#include "recipe.h"
#include "market.h"

using namespace std;

class Player
{
public:

	string name;
	double cash = 200;
	Recipe recipe;
	Recipe stock;
	double price = 6.00;

	string getRecipeListing(string ingredient) {
		string listing = "  (R" + ingredient.substr(0,1) + ")";

		stringstream stream;
		stream << fixed << setprecision(0) << recipe.amount[ingredient];
		string amount = stream.str();

		while (amount.length() < 5) {
			amount = " " + amount;
		}
		listing += amount + " " + recipe.smallUnit[ingredient] + " ";

		while (listing.length() < 26) {
			listing += ".";
		}
		listing += " " + ingredient;

		while (listing.length() < 39) {
			listing += " ";
		}

		return listing;

	}

	string getStockListing(string ingredient) {
		string listing = "  (B" + ingredient.substr(0, 1) + ") " + ingredient + " ";

		stringstream stream;
		stream << fixed << setprecision(2) << stock.amount[ingredient];
		string amount = stream.str();

		while (listing.length() + amount.length()+1 < 31) {
			listing += ".";
		}

		listing += " " + amount + " " + stock.bulkUnit[ingredient];

		while (listing.length() < 39) {
			listing += " ";
		}

		return listing;
	}

	string getPriceListing() {

		stringstream stream;
		stream << fixed << setprecision(2) << price;
		string priceString = stream.str();

		while (priceString.length() < 13) {
			priceString += " ";
		}

		return priceString;
	}

	string getCashListing() {
		stringstream stream;
		stream << fixed << setprecision(2) << cash;
		string cashString = stream.str();

		while (cashString.length() < 24) {
			cashString += " ";
		}

		return cashString;

	}

	string buyIngredient(string ingredient, double amount, Market market) {

		double cost = market.prices.amount[ingredient] * amount;

		if (cash - cost < 0) {
			return "You can't afford that.";
		}
		else {
			cash += -cost;
			stock.amount[ingredient] += amount;
			return "More " + ingredient + " has been purchased.";
		}

	}

	Player() {
		recipe.amount["Flour"] = 550;
		recipe.amount["Yeast"] = 10;
		recipe.amount["Water"] = 300;
		recipe.amount["Tomatoes"] = 5;
		recipe.amount["Garlic"] = 15;
		recipe.amount["Salt"] = 20;
		recipe.amount["Oregano"] = 5;
		recipe.amount["Mozzerella"] = 200;
		recipe.amount["Pepperoni"] = 25;

		stock.amount["Flour"] = 20;
		stock.amount["Yeast"] = 0.5;
		stock.amount["Water"] = 200;
		stock.amount["Tomatoes"] = 50;
		stock.amount["Garlic"] = 200;
		stock.amount["Salt"] = 2;
		stock.amount["Oregano"] = 0.5;
		stock.amount["Mozzerella"] = 5;
		stock.amount["Pepperoni"] = 4;
	}
};