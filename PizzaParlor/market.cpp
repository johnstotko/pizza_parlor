#include <string>
#include <sstream>
#include <iomanip>
#include "recipe.h"
#include "market.h"

using namespace std;

string Market::getIngredientListing(string ingredient) {
	string listing = "...................";
	listing.replace(0, ingredient.length(), ingredient + " ");

	stringstream stream;
	stream << fixed << setprecision(2) << prices.amount[ingredient];
	string price = stream.str();

	listing.replace(listing.length() - price.length() - 1, listing.length() - 1, " " + price);

	listing = "     " + listing + " $/" + prices.bulkUnit[ingredient];

	while (listing.length() < 37) {
		listing += " ";
	}

	return listing;
}

void Market:: driftPrices() {
	// TODO: Drift prices, not nessisary right now.
}

Market::Market() {
	prices.amount["Flour"] = 0.81;
	prices.amount["Yeast"] = 47.70;
	prices.amount["Water"] = 0.22;
	prices.amount["Tomatoes"] = 0.17;
	prices.amount["Garlic"] = 12.44;
	prices.amount["Salt"] = 0.65;
	prices.amount["Oregano"] = 51.67;
	prices.amount["Mozzerella"] = 7.48;
	prices.amount["Pepperoni"] = 25.76;

}