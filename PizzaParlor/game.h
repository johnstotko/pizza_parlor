#ifndef GAME_H
#define GAME_H

class Game
{
public:
	const int EXIT = 0;
	const int MAINMENU = 1;
	const int NEWGAME = 2;
	const int LOADGAME = 3;
	const int OPTIONS = 4;
	const int CREDITS = 5;

	int currentState = 1;

	void setCurrentState();
	int getCurrentState();

};

#endif

