#pragma once
#ifndef RECIPE_H
#define RECIPE_H

#include <map>
#include <string>
#include <vector>

using namespace std;

class Recipe
{
public:
	vector<string> ingred;
	map<string, double> amount;
	map<string, string> smallUnit;
	map<string, string> bulkUnit;
	map<string, double> marketToPlayerUnitConvert;
	map<string, string> abbreviation;

	string getRandomIngredient();

	Recipe();
};

#endif