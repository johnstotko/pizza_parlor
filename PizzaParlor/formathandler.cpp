#include "formathandler.h"
#include <sstream>
#include <iomanip>
#include <string>
using namespace std;

string FormatHandler::doubleToDollar(double number) {
	stringstream stream;
	stream << fixed << setprecision(2) << number;
	return stream.str();
}